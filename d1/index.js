console.log("Hello World")

// What are conditional statements?
// Conditional Statements allows us to control the flow of our program
// It allows us to run a statement/instructions if a condition is met or run another separate instruction if otherwise

// If, else if, and else statement


let numA = -1;

// if statement
	// executes a statement if a specified condition is true

if(numA <0){
	console.log("numA is less than 0");
}

/*
	Syntax:
		if(condition){
			statment
		}

*/

// the result of the expression added in the if's condition must result to true, else, the statement inside the if() will not run

console.log(numA<0);//true


numA = 0;

if (numA < 0){
	console.log("Hello Again if numA is 0");
}

console.log(numA<0);//will not run because condition is false

let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}

// else if Clause

/*
	- Executes a statement if previous conditions are false and if the condition is true
	- The "else If" clase us optional and can be added to capture additonal conditions to change the flow of a program 

*/
let numH = 1;

if (numA < 0){
	console.log("If statement will run");
} else if (numH > 0){
	console.log("Else statement will run");
}


// if the "if" condition was passed and run, we will no longer evaluate the else if() and we end the process there 

numA = 1;

if (numA > 0 ){
	console.log("The number is greater than zero, the if statement runs");
}else if (numH >0){
	console.log("I am the else if statement");
}


// another example

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City!");
} else if (city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// else Statement

	// The else statement will execute if all other condtions are false
	// is optional and can be added to capture any other result to change the flow of a program

numA = -1;
numH = 0;

if (numA > 0){
	console.log("Hello");
} else if(numH === 0){
	console.log("World");
} else{
	console.log("I will execute if all other conditions are false");
}




// Else statements should only be added if there is a preceeding "if" conditions.
// Else statements by itself will not work, however, "if" statements will work even if there is no else statement

// else {
// 	console.log("Will not run without an if");
// }

// another example

// else if (numH === 0){
// 	console.log("Hi, I'm else if");
// } else{
// 	console.log("Hi, I'm else");
// }

// there should be a preceeding if() fist




// if, else if, and else Statement with function

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windspeed){

	if (windspeed < 30){
		return 'Not a typhoon yet!';
	}
	else if (windspeed <= 61){
		return 'Tropical depression detected';
	}
	else if (windspeed >= 62 && windspeed <= 88){
		return "Tropical storm detected.";
	}
	else if (windspeed >= 89 || windspeed <= 177){
		return "Severe tropical storm detected.";
	}
	else {
		return "Typhoon detected!";
	}

}

message = determineTyphoonIntensity(110);
console.log(message);

if (message === "Severe tropical storm detected."){
	console.warn(message);
}
// console.warn is a good way to print warnings in our console that could help us developers act on certain output within our code

/*
	Mini Activity 1
	Create a function that can check whether a number is odd or even called oddOrEvenChecker with one parameter

	**A number must be provided as an argument
	**Use the if and else statement
	**It should not return anything
	**There should be an alert if the condition is met
	**invoke and pass 1 argument to the oddOrEvenChecker function
*/

// function oddOrEvenChecker(number){
// 	if (number % 2 === 0){
// 		alert(number + " is even!");
// 	}
// 	else {
// 		alert(number + " is odd!");
// 	}
// }

// oddOrEvenChecker(5);

function ageChecker(age){
	if (age >= 18){
		alert("You're " + age + " you're allowed to drink");
		return (age >= 18);
		// return (true);

	}
	else {
		alert("You're " + age + " you're underage!");
		return (age >= 18);
		// return false
	}
}
	isAllowedToDrink = ageChecker(18);
	console.log("allowed to drink: ");
	console.log(isAllowedToDrink);
	


// Truthy and Falsy values

	/*
		In JavaScript, a thruthy value is a value that is considered true when encountered in a BOOLEAN context

		-Values are considered true unless defined otherwise
		-Falsy values/exception for truthy:
			1. false
			2. 0 
			3. -0
			4. ""
			5. null
			6. undefined
			7. Nan

	*/

	// Truthy Examples

	/*
		if the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
		- Expressions are any unit of code that can be evaluated to a value
	*/

	if(true){
		console.log("True is Truthy");
	}
	if(1){
		console.log("1 is Truthy");
	}
	if([]){
		console.log("[] empty array is Truthy");
	}
	if("Jap"){
		console.log("Is Jap Truthy?");
	}

	// Falsy Values

	if(false){
		console.log("Falsy");
	}
	if(0){
		console.log("Falsy");
	}
	if(undefined){
		console.log("Falsy");
	}
	else {
		console.log("undefined is falsy")
	}

	// Conditional Ternary Operator
	// Ternary operator is used as a shorter alernative to if-else statements
	// It is also able to implicitly return a value

	// syntax:
	// (condition) ? ifTrue : ifFalse;

	let age = 17;
	let result = age < 18? "Underage" : "Legal Age"
	console.log(result)

	/*
		let result = if (age<18){
			return "Underage";
		}
		else {
			return "Legal Age";
		}

		console.log(result);
	*/


	// Switch Statement
	// the switch statement evaluates an expression and matches the expression's value to a CASE clause
	// The switch will then execute the statements associated with that case, as well as statements in case that follow the matching case

	// .toLowerCase() function/method will change the input received from the prompt into all lowercase letters --- match with the switch case conditions if the user inputs capitalized or uppercase letter

	// the break statement 

	// Syntax
	/*
		switch (expression){
	
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day){

		case 'monday':
			console.log("The color of the day is red!");
			break;
		case 'tuesday':
			console.log("The color of the day is orange!");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow!");
			break;
		case 'thursday':
			console.log("The color of the day is purple!");
			break;
		case 'friday':
			console.log("The color of the day is pink!");
			break;
		case 'saturday':
			console.log("The color of the day is blue!");
			break;
		case 'sunday':
			console.log("The color of the day is green!");
			break;
		default:
			console.log("Please input a valid day");
			break;
			
	}
	/*
		Mini activity
		add the remaining days as cases for our switch statement
			-thursday
			-friday
			-saturday
			-sunday

			You can also customize the message per case that will be logged in the console 
			Take a screenshot of your console for at least once of the new cases

	*/

	// Mini Activity 4

	function determineBear(bearNumber){
		let bear;
		switch (bearNumber){

		case 1:
			console.log("Hi, I'm Amy!");
			break;
		case 2:
			console.log("Hi, I'm Lulu!");
			break;
		case 3:
			console.log("Hi, I'm Morgan!");
			break;
		default:
			bear = bearNumber + " is out of bounds!";
			break;
		}
	}
	determineBear(2);

// Try-Cath-Finally Statement

		// try catch statements are commonly used for error handling


	function showIntensityAlert(windspeed){
		try{

			alerat(determineTyphoonIntensity(windspeed));
		
		} 
		catch(error){

			console.log(typeof error);
			console.warn(error.message);

		}
		finally{

			alert("Intensity updates will show new alert!");

		}
	}

	showIntensityAlert(56)

	console.log("Hi");


